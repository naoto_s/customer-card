class CustomerCard {
	static int nextId = 1001;
	int id; //ID番号
	String name; //氏名
	String address; //住所

	CustomerCard(String name, String address){
		this.id = CustomerCard.nextId;
		CustomerCard.nextId++;
		this.name = name;
		this.address = address;
	}

	CustomerCard(String name){
		this(name, "");
	}

	void printInfo() {
		System.out.println("ID:" + this.id );
		System.out.println("氏名:" + this.name );
		System.out.println("住所:" + this.address );
	}
}

class CustomerManager {
	public static void main(String[] args) {
		CustomerCard[] cards = new CustomerCard[100];
		cards[0] = new ShoeShopCustomerCard("山田太郎", "東京都", 26.5);

		cards[1] = new ShoeShopCustomerCard("佐藤華子", "神奈川県", 24.5);

		cards[2] = new ShoeShopCustomerCard("鈴木健司", "茨城県", 26.0);

		cards[3] = new HatShopCustomerCard("渡辺進", "東京都", 57.0);

		for(int i = 0; i < cards.length; i++) {
			if(cards[i] == null) {
				break;
			}

			System.out.println(i + "番目の顧客カードに記載の情報");
			cards[i].printInfo();
			System.out.println("==============");
		}
	}
}
